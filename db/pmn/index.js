'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/* Mongoose schema */
var electricityDataSchema = new Schema({
    channel: { type:String, require:true  },
    kwh: { type:String },
    usageKwh: { type:String },
    timestamp: { type:Date, require:true },
    archiveKey: { type:String },
    archiveFlag: { type:Boolean, require:true, default:false },
    refUsage: { type:Schema.Types.ObjectId, ref:'pmn.ElectricityData' },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var electricityDailyDataSchema = new Schema({
    channel: { type:String, require:true  },
    sumKwh: { type:String },
    archiveDate: { type:Date, require:true },
    archiveKey: { type:String },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var electricityMonthlyDataSchema = new Schema({
    channel: { type:String, require:true  },
    sumKwh: { type:String },
    archiveDate: { type:Date, require:true },
    archiveKey: { type:String },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var electricityYearlyDataSchema = new Schema({
    channel: { type:String, require:true  },
    sumKwh: { type:String },
    archiveDate: { type:Date, require:true },
    archiveKey: { type:String },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var electricitySummaryDataSchema = new Schema({
    channel: { type:String, require:true  },
    sumKwh: { type:String },
    archiveDate: { type:Date, require:true },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var waterDataSchema = new Schema({
    channel: { type:String, require:true  },
    unit: { type:String },
    usageUnit: { type:String },
    timestamp: { type:Date, require:true },
    archiveKey: { type:String },
    archiveFlag: { type:Boolean, require:true, default:false },
    refUsage: { type:Schema.Types.ObjectId, ref:'pmn.WaterData' },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var waterDailyDataSchema = new Schema({
    channel: { type:String, require:true  },
    sumUnit: { type:String },
    archiveDate: { type:Date, require:true },
    archiveKey: { type:String },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var waterMonthlyDataSchema = new Schema({
    channel: { type:String, require:true  },
    sumUnit: { type:String },
    archiveDate: { type:Date, require:true },
    archiveKey: { type:String },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var waterYearlyDataSchema = new Schema({
    channel: { type:String, require:true  },
    sumUnit: { type:String },
    archiveDate: { type:Date, require:true },
    archiveKey: { type:String },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var waterSummaryDataSchema = new Schema({
    channel: { type:String, require:true  },
    sumUnit: { type:String },
    archiveDate: { type:Date, require:true },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

/* Schema middleware */
electricityDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

electricityDailyDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

electricityMonthlyDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

electricityYearlyDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

electricitySummaryDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

waterDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

waterDailyDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

waterMonthlyDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

waterYearlyDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

waterSummaryDataSchema.pre('update', function() {
    if (!this.createDate) this.update({},{$set: {createDate:new Date()}});
    this.update({},{$set: {updateDate:new Date()}});
});

/* Export model */
exports.ElectricityData = mongoose.model('pmn.ElectricityData', electricityDataSchema);
exports.ElectricityDailyData = mongoose.model('pmn.ElectricityDailyData', electricityDailyDataSchema);
exports.ElectricityMonthlyData = mongoose.model('pmn.ElectricityMonthloyData', electricityMonthlyDataSchema);
exports.ElectricityYearlyData = mongoose.model('pmn.ElectricityYearlyData', electricityYearlyDataSchema);
exports.ElectricitySummaryData = mongoose.model('pmn.ElectricitySummaryData', electricitySummaryDataSchema);
exports.WaterData = mongoose.model('pmn.WaterData', waterDataSchema);
exports.WaterDailyData = mongoose.model('pmn.WaterDailyData', waterDailyDataSchema);
exports.WaterMonthlyData = mongoose.model('pmn.WaterMonthlyData', waterMonthlyDataSchema);
exports.WaterYearlyData = mongoose.model('pmn.WaterYearlyData', waterYearlyDataSchema);
exports.WaterSummaryData = mongoose.model('pmn.WaterSummaryData', waterSummaryDataSchema);
