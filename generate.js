'use strict';

/* Module dependencies */
var async = require('async');
var moment = require('moment');
var config = require('config'),
    conf = config.get('app');
var db = require('./db');

/**********************
* DB Generate Process *
**********************/
var process = function () {
    try {
        var currentDate = moment();
        var startDate = currentDate.clone()
            .subtract(conf.generate.subtract.years, 'years')
            .subtract(conf.generate.subtract.months, 'months')
            .subtract(conf.generate.subtract.days, 'days');

        var runningDate = startDate.clone();

        async.whilst(
            function() { return runningDate.isSameOrBefore(currentDate); },
            function(done) {

                console.log(runningDate.toString());
                async.series([
                    function(callback) {
                        generateElectricityData(runningDate, callback);
                    },
                    function(callback) {
                        generateWaterData(runningDate, callback);
                    }
                ], function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        runningDate.add(1, 'hours');
                        done(null);
                    }
                });
            },
            function(err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('DONE');
                }
            }
        );

    } catch (err) {
        console.log(err);
    }
}

module.exports.process = process;

/* Private methods */
var generateElectricityData = function(timestamp, done) {
    try {
        var randElectricityDataObj = getRandomElectricityData();
        var channelList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18'];
        async.each(channelList, function(channel, cb) {

            var electricityData = randElectricityDataObj.electricity[channel];
            var electricityFixed = conf.generate.fixed.electricity[channel];
            saveElectricityData(timestamp, channel, electricityFixed, electricityData.kwh, function(err) {
                if (err) {
                    cb(err);
                } else {
                    cb(null);
                }
            });
        },
        function(err) {
            if (err) {
                done(err);
            } else {
                done(null);
            }
        });
    } catch (err) {
        done(err);
    }
}

var generateWaterData = function(timestamp, done) {
    try {
        var randWaterDataObj = getRandomWaterData();
        var channelList = ['1', '2', '3'];
        async.each(channelList, function(channel, cb) {

            var waterData = randWaterDataObj.water[channel];
            var waterFixed = conf.generate.fixed.water[channel];
            saveWaterData(timestamp, channel, waterFixed, waterData.unit, function(err) {
                if (err) {
                    cb(err);
                } else {
                    cb(null);
                }
            });
        },
        function(err) {
            if (err) {
                done(err);
            } else {
                done(null);
            }
        });
    } catch (err) {
        done(err);
    }
}

var getRandomElectricityData = function() {
    try {
        return {
            'electricity': {
                '1': { 'kwh': ((randomInt(20, 40) * 10) / 240).toFixed(2)},
                '2': { 'kwh': ((randomInt(20, 40) * 10) / 240).toFixed(2)},
                '3': { 'kwh': ((randomInt(20, 40) * 10) / 240).toFixed(2)},
                '4': { 'kwh': ((randomInt(20, 40) * 10) / 240).toFixed(2)},
                '5': { 'kwh': ((randomInt(20, 40) * 10) / 240).toFixed(2)},
                '6': { 'kwh': ((randomInt(20, 40) * 10) / 240).toFixed(2)},
                '7': { 'kwh': ((randomInt(20, 40) * 0.61) / 240).toFixed(2)},
                '8': { 'kwh': ((randomInt(20, 40) * 0.61) / 240).toFixed(2)},
                '9': { 'kwh': ((randomInt(20, 40) * 0.61) / 240).toFixed(2)},
                '10': { 'kwh': ((randomInt(20, 40) * 0.61) / 240).toFixed(2)},
                '11': { 'kwh': ((randomInt(20, 40) * 6.5) / 240).toFixed(2)},
                '12': { 'kwh': ((randomInt(20, 40) * 6.5) / 240).toFixed(2)},
                '13': { 'kwh': ((randomInt(20, 40) * 11.48) / 240).toFixed(2)},
                '14': { 'kwh': ((randomInt(20, 40) * 11.48) / 240).toFixed(2)},
                '15': { 'kwh': ((randomInt(20, 40) * 0.52) / 240).toFixed(2)},
                '16': { 'kwh': ((randomInt(20, 40) * 0.52) / 240).toFixed(2)},
                '17': { 'kwh': ((randomInt(20, 40) * 0.52) / 240).toFixed(2)},
                '18': { 'kwh': ((randomInt(20, 40) * 6.8) / 240).toFixed(2)}
            }
        };
    } catch (err) {
        console.log(err);
    }
}

var getRandomWaterData = function() {
    try {
        return {
            'water': {
                '1': { 'unit': ((randomInt(30, 50) * 0.0361) / 30).toFixed(2)},
                '2': { 'unit': ((randomInt(60, 100) * 0.0015) / 30).toFixed(2)},
                '3': { 'unit': ((randomInt(75, 100) * 0.0017) / 30).toFixed(2)}
            }
        };
    } catch (err) {
        console.log(err);
    }
}

var saveElectricityData = function(timestamp, channel, fixedKwh, usageKwh, callback) {
    try {
        var archiveDate = moment([
            timestamp.year(),
            timestamp.month(),
            timestamp.date(),
            timestamp.hour()
        ]);
        var archiveKey = archiveDate.format('YYYYMMDDHH');
        /* save electricity data */
        var _electricityData = new db.PMN.ElectricityData();
        _electricityData.channel = channel;
        _electricityData.kwh = fixedKwh;
        _electricityData.usageKwh = usageKwh;
        _electricityData.timestamp = timestamp;
        _electricityData.archiveKey = archiveKey;
        _electricityData.refUsage = null;
        _electricityData.save(function(err) {
            if (err) {
                callback(err);
            } else {
                callback(null);
            }
        });
    } catch (err) {
        callback(err);
    }
}

var saveWaterData = function(timestamp, channel, fixedUnit, usageUnit, callback) {
    try {
        var archiveDate = moment([
            timestamp.year(),
            timestamp.month(),
            timestamp.date(),
            timestamp.hour()
        ]);
        var archiveKey = archiveDate.format('YYYYMMDDHH');
        /* save electricity data */
        var _waterData = new db.PMN.WaterData();
        _waterData.channel = channel;
        _waterData.unit = fixedUnit;
        _waterData.usageUnit = usageUnit;
        _waterData.timestamp = timestamp;
        _waterData.archiveKey = archiveKey;
        _waterData.refUsage = null;
        _waterData.save(function(err) {
            if (err) {
                callback(err);
            } else {
                callback(null);
            }
        });
    } catch (err) {
        callback(err);
    }
}

var randomInt = function(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
}
