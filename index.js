#!/usr/bin/env node

/* Module dependencies */
var mongoose = require('mongoose');
var config = require('config'),
    dbconf = config.get('db');

function start() {
    try {
        /* Connect to database */
        var conn = mongoose.connection;
        conn.on('error', function(err) {
            try {
                console.log('DB Connection error');
            } catch (err) {
                console.log(err);
            }
        });

        conn.once('connected', function() {
            try {
                console.log('DB Connected');

                /* Start generate process */
                require('./generate').process();

            } catch (err) {
                console.log(err);
            }
        });

        conn.on('disconnected', function() {
            try {
                console.log('DB Disconnected');
            } catch (err) {
                console.log(err);
            }
        });

        mongoose.connect('mongodb://' + dbconf.host + '/' + dbconf.name );

    } catch (err) {
        console.log(err);
    }
}

/* Start process */
start();
